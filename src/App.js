import './App.css';
import { useState } from 'react';

function App() {

  let post = '테스트 글제목';
  let [글제목, 글제목변경] = useState(['남자 코트 추천','강남 우동맛집','파이썬 독학']);
  let [따봉,따봉변경] =useState([0,1,2]);
  let [modal, setModal] = useState(true);
  let [titleNo, setTitleNo] = useState(2);
  let [input, setInput] = useState('');

    // 각각 클릭시 따봉 수 변경
    function plusThumbs(i){
        let copy = [...따봉];
        copy[i] = copy[i] + 1;
        따봉변경(copy);
    }

    function remove(i){
        let copy  = [...글제목];
        copy.splice(i,1);
        글제목변경(copy);
    }

    function add(e){
        let copy = [input, ...글제목];
        글제목변경(copy);
        let copy2 = [0,...따봉];
        따봉변경(copy2);
        setInput('');
    }





  return (
      <div className="App">
        <div className='black-nav'>
          <h4>ReactBlog</h4>
        </div>


        <button onClick={() => {
          let copy = [...글제목];
          copy[0] ='여자 코트추천';
          글제목변경(copy);
        }}>글수정</button>

        <button onClick={() => {
          let copy = [...글제목];
          copy.sort();
          글제목변경(copy);
        }}>정렬하기</button>

          {
              글제목.map((element,i)=>{
                  return(
                  <div className='list' key={i}>
                      <h4>
                          <span onClick={()=> {setModal(!modal); setTitleNo(i)}}>{글제목[i]}</span>
                          <span onClick={()=>plusThumbs(i)}>👍</span>{따봉[i]}
                      </h4>
                      <p>9월 9일 발행 <span style={{background:"yellowgreen"}} onClick={() => remove(i)}>삭제하기</span>
                      </p>
                  </div>
                  )
              })
          }

          <input
              onChange={(e) => {setInput(e.target.value)}}
              type="text"
              value={input}
          /> <span onClick={add}>등록하기</span>

          {
              modal ? <Modal titleNo={titleNo} title={글제목} color={'skyblue'}/> : null
          }

      </div>
  );
}


// 컴포넌트 만드는 방법
// 1. 대문자 시작으로 함수 만들기
// 2. return() 안에 html 담기
// 3. <함수명> </함수명> 쓰기

// 컴포넌트 만들어야 하는 상황
// 1. 반복적인 html을 축약할 때
// 2. 큰 페이지들
// 3. 자주 변경되는 것들
function Modal(props) {
  return(
      <div className="modal" style={{background : props.color}}>
        <h4>{props.title[props.titleNo]}</h4>
        <p>날짜</p>
        <p>상세내용</p>
      </div>
  )
}



export default App;
